package com.reachout.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoggedInUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in_user);
    }
}
